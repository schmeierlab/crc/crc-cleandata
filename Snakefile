import glob, os, os.path, datetime
from os.path import join
from snakemake.utils import report

## define global Singularity image for reproducibility
## needs --use-singularity to run all jobs in container
singularity: "docker://continuumio/miniconda3:4.5.4"

## =============================================================================
## LOAD VARIABLES FROM CONFIGFILE
## config-file needs to be submitted on command-line via --configfile
            
BASEDIR = config["basedir"]

# results
SAMPLEDIR   = config["sampledir"]
ADAPTDIR    = config["adaptdir"]
TRIMDIR     = config["trimdir"]
SORTDIR     = config["sortdir"]
QCDIR       = config["qcdir"]
QCMULTIDIR  = config["multiqcdir"]
QCMULTIFILE = config["multiqcfile"]
MERGEDIR    = config["mergedir"]

TRIM = config["trim"]
LEN  = config["length"]
# inputs
ADAPTERFILE = config["adapterfile"]

# logs and programs
LOGDIR = config["logdir"]
BENCHMARKDIR = config["benchmarkdir"]
WRAPPERDIR = config["wrapperdir"]
SCRIPTDIR = config["scriptdir"]
##------------------------------------------

SAMPLES, = glob_wildcards(join(SAMPLEDIR, '{sample,.+_L\d\d\d}_R1_001.fastq.gz'))
print(len(SAMPLES))

## Pseudo-rule to state the final targets, so that the whole
## workflow is run.
rule all:
    input:
        #QCMULTIFILE
        expand(join(SORTDIR, '{sample}.done'), sample=SAMPLES)

## Adaptertrim
rule adaptertrim:
    input:
        r1 = join(SAMPLEDIR, '{sample}_R1_001.fastq.gz'),
        r2 = join(SAMPLEDIR, '{sample}_R2_001.fastq.gz')
    output:
        out1 = join(ADAPTDIR, '{sample}_R1_001.fastq.gz'),  ## will be removed when not used anymore 
        out2 = join(ADAPTDIR, '{sample}_R2_001.fastq.gz')  ## will be removed when not used anymore 
    log:
        log1 = join(LOGDIR, "adaptertrim/{sample}.stdout"),
        log2 = join(LOGDIR, "adaptertrim/{sample}.stderr")
    benchmark:
        join(BENCHMARKDIR,"{sample}.adaptertrim.txt")
    params:
        extra="", ##r"-k 0", ## -C 100", ## TODO: need to delete -C in production
        a=ADAPTERFILE
    conda:
        "envs/eautils.yaml"
    shell:
        "nice fastq-mcf {params.extra} -o {output.out1} -o {output.out2} {params.a} <(gunzip -c {input.r1}) <(gunzip -c {input.r2}) > {log.log1} 2> {log.log2}" 


rule dynamictrim:
    input:
        r1 = join(ADAPTDIR, '{sample}_R1_001.fastq.gz'),
        r2 = join(ADAPTDIR, '{sample}_R2_001.fastq.gz')
    output:
        touch(join(TRIMDIR, '{sample}.done'))
    log:
        log1 = join(LOGDIR, "dynamictrim/{sample}.stdout"),
        log2 = join(LOGDIR, "dynamictrim/{sample}.stderr")
    benchmark:
        join(BENCHMARKDIR,"{sample}.dynamictrim.txt")
    params:
        extra = r"",
        p = TRIM,
        outdir = join(TRIMDIR, '{sample}')
    conda:
        "envs/solexaqa.yaml"
    shell:
        "mkdir -p {params.outdir};"
        "nice ./src/scripts/SolexaQA++ dynamictrim {params.extra} {input.r1} {input.r2} -p {params.p} -d {params.outdir} > {log.log1} 2> {log.log2}"

        
rule lengthsort:
    input:
        join(TRIMDIR, '{sample}.done')
    output:
        touch(join(SORTDIR, '{sample}.done'))
    log:
        log1 = join(LOGDIR, "lengthsort/{sample}.stdout"),
        log2 = join(LOGDIR, "lengthsort/{sample}.stderr")
    benchmark:
        join(BENCHMARKDIR,"{sample}.lengthsort.txt")
    params:
        extra=r"", 
        l=LEN,
        r1 = join(join(TRIMDIR, '{sample}'), '{sample}_R1_001.fastq.trimmed.gz'),
        r2 = join(join(TRIMDIR, '{sample}'), '{sample}_R2_001.fastq.trimmed.gz'),
        outdir = join(SORTDIR, '{sample}')
    conda:
        "envs/solexaqa.yaml"
    shell:
        "mkdir -p {params.outdir};"
        "nice ./src/scripts/SolexaQA++ lengthsort {params.extra} {params.r1} {params.r2} --length {params.l} --directory {params.outdir} > {log.log1} 2> {log.log2}"


rule fastqc:
    input:
        join(SORTDIR, '{sample}.done')
    output:
        join(QCDIR, '{sample}.done')
    log:
        log1 = join(LOGDIR, "fastqc/{sample}.stdout"),
        log2 = join(LOGDIR, "fastqc/{sample}.stderr")
    benchmark:
        join(BENCHMARKDIR,"{sample}.fastqc.txt")
    params:
        extra=r"--noextract",
        r1 = join(join(SORTDIR, '{sample}'), '{sample}_R1_001.fastq.trimmed.paired.gz'),
        r2 = join(join(SORTDIR, '{sample}'), '{sample}_R2_001.fastq.trimmed.paired.gz'),
        outdir = join(QCDIR, '{sample}')
    threads: 2
    conda:
        "envs/fastqc.yaml"
    shell:
        "mkdir -p {params.outdir};"
        "fastqc {params.extra} --threads {threads} -o {params.outdir} {params.r1} {params.r2} > {log.log1} 2> {log.log2}"


rule multiqc:
    input:
        expand(join(QCDIR, '{sample}.done'), sample=SAMPLES)
    output:
        QCMULTIFILE
    log:
        log1 = join(LOGDIR, "multiqc/multiqc.stdout"),
        log2 = join(LOGDIR, "multiqc/multiqc.stderr")
    params:
        extra=r"--force", # --quiet
        outdir = QCMULTIDIR,
        indir = QCDIR
    conda:
        "envs/multiqc.yaml"
    shell:
        "multiqc {params.extra} --filename {output} --outdir {params.outdir} {params.indir} > {log.log1} 2> {log.log2}"

