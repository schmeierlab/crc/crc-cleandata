__author__ = "Sebastian Schmeier"
__copyright__ = "Copyright 2017, Sebastian Schmeier"
__email__ = "s.schmeier@gmail.com"
__license__ = "MIT"
from snakemake.shell import shell

log = snakemake.log_fmt_shell(stdout=False, stderr=True)

shell("fastq-mcf {snakemake.params.extra} "
      "-o {snakemake.output.out1} -o {snakemake.output.out2} "
      "{snakemake.params.a} {snakemake.input.r1} {snakemake.input.r2} > {log}")
