DIR: src/wrapper
AUTHOR: Sebastian Schmeier (s.schmeier@gmail.com)

# Wrapper

For information on available wrappers see [here](https://bitbucket.org/snakemake/snakemake-wrappers).
More detailed information on the use of wrappers see [here](https://snakemake-wrappers.readthedocs.io/en/stable/).
 
