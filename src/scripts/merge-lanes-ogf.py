#!/usr/bin/env python
"""
NAME: merge-males-ogf.py
=========

DESCRIPTION
===========

INSTALLATION
============

USAGE
=====

VERSION HISTORY
===============

0.0.1   2017/12/09    Initial version.

LICENCE
=======
2017, copyright Sebastian Schmeier (s.schmeier@gmail.com)

template version: 1.8 (2017/10/06)
"""
from signal import signal, SIGPIPE, SIG_DFL
import sys
import os
import os.path
import argparse
import csv
import collections
import gzip
import bz2
import zipfile
import time
import glob
import subprocess

# When piping stdout into head python raises an exception
# Ignore SIG_PIPE and don't throw exceptions on it...
# (http://docs.python.org/library/signal.html)
signal(SIGPIPE, SIG_DFL)

__version__ = '0.0.1'
__date__ = '2017/12/09'
__email__ = 's.schmeier@gmail.com'
__author__ = 'Sebastian Schmeier'

# For color handling on the shell
try:
    from colorama import init, Fore, Style
    # INIT color
    # Initialise colours for multi-platform support.
    init()
    reset=Fore.RESET
    colors = {'success': Fore.GREEN, 'error': Fore.RED, 'warning': Fore.YELLOW, 'info':''}
except ImportError:
    sys.stderr.write('colorama lib desirable. Install with "conda install colorama".\n\n')
    reset=''
    colors = {'success': '', 'error': '', 'warning': '', 'info':''}

def alert(atype, text, log):
    textout = '%s [%s] %s\n' % (time.strftime('%Y%m%d-%H:%M:%S'),
                                atype.rjust(7),
                                text)
    log.write('%s%s%s' % (colors[atype], textout, reset))
    if atype=='error': sys.exit()
        
def success(text, log=sys.stdout):
    alert('success', text, log)
    
def error(text, log=sys.stderr):
    alert('error', text, log)
    
def warning(text, log=sys.stderr):
    alert('warning', text, log)
    
def info(text, log=sys.stdout):
    alert('info', text, log)  

    
def parse_cmdline():
    """ Parse command-line args. """
    ## parse cmd-line -----------------------------------------------------------
    description = 'Read samplename and merge all R1 reads and R2 reads into one file each in the same order.'
    version = 'version %s, date %s' % (__version__, __date__)
    epilog = 'Copyright %s (%s)' % (__author__, __email__)

    parser = argparse.ArgumentParser(description=description, epilog=epilog)

    parser.add_argument('--version',
                        action='version',
                        version='%s' % (version))

    parser.add_argument(
        'sample',
        metavar='SAMPLE',
        help= 'Sample id to merge lanes.')
    parser.add_argument("--dir",
        metavar='DIR',
        default="./",
        help= 'Directory with folders for cleaned samples. [default: current dir]')
    parser.add_argument("--outdir",
        metavar='OUTDIR',
        default="merged",
        help= 'Directory with folders for cleaned samples. [default: merged]')
    parser.add_argument("--pattern_r1",
        metavar='PATTERN_R1',
        default="*R1*fastq*paired.gz",
        help= 'Read 1 attern to search for in sample-dir. [default: *R1*fastq*paired.gz]')
    parser.add_argument("--pattern_r2",
        metavar='PATTERN_R2',
        default="*R2*fastq*paired.gz",
        help= 'Read 2 attern to search for in sample-dir. [default: *R2*fastq*paired.gz]')

    
    # if no arguments supplied print help
    if len(sys.argv)==1:
        parser.print_help()
        sys.exit(1)
    
    args = parser.parse_args()
    return args, parser


def main():
    """ The main funtion. """
    args, parser = parse_cmdline()

    # search files r1

    info('Identify files to merge...')
    s1 = '{}/*{}*/{}*'.format(args.dir, args.sample, args.pattern_r1)
    s2 = '{}/*{}*/{}*'.format(args.dir, args.sample, args.pattern_r2)
    r1 = glob.glob(s1)
    r1.sort()
    r2 = glob.glob(s2)
    r2.sort()
    assert len(r1) == len(r2)

    
    info('R1 files ({}):'.format(len(r1)))
    for s in r1:
        info('{}'.format(s))
    info('R2 files ({}):'.format(len(r2)))
    for s in r2:
        info('{}'.format(s))
        
    cmd1 = 'mkdir -p {}; cat {} > {}/{}_R1.fastq.gz'.format(args.outdir, ' '.join(r1), args.outdir, args.sample)
    cmd2 = 'mkdir -p {}; cat {} > {}/{}_R2.fastq.gz'.format(args.outdir, ' '.join(r2), args.outdir, args.sample)
    info('Run cmd: {}'.format(cmd1))
    try:
        retcode = subprocess.call(cmd1, shell=True)
        if retcode < 0:
            error("Child was terminated by signal: {}".format(-retcode))
        else:
            success("Child returned: {}".format(retcode))
    except OSError as e:
            error("Execution failed: {}".format(e))
    
    info('Run cmd: {}'.format(cmd2))
    try:
        retcode = subprocess.call(cmd2, shell=True)
        if retcode < 0:
            error("Child was terminated by signal: {}".format(-retcode))
        else:
            success("Child returned: {}".format(retcode))
    except OSError as e:
            error("Execution failed: {}".format(e))
        
    return


if __name__ == '__main__':
    sys.exit(main())

