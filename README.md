# PROJECT: CRC data clean
- AUTHOR: Sebastian Schmeier (s.schmeier@gmail.com) 
- DATE: 2017-12-01

## INSTALL

```bash
# Install miniconda
# LINUX:
curl -O https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh
# MACOSX:
curl -O https://repo.continuum.io/miniconda/Miniconda3-latest-MacOSX-x86_64.sh
bash Miniconda3-latest-MacOSX-x86_64.sh

# Add conda dir to PATH
echo 'export PATH="~/miniconda3/bin:$PATH"' >> ~/.bashrc
echo 'export PATH="~/miniconda3/bin:$PATH"' >> ~/.zshrc

# Make env
# this environment contains the bare base of what is required to run snakemake
conda env create --name snakemake --file envs/snakemake.yaml
source activate snakemake
```

## Run standard pipeline

```bash
snakemake --use-conda --use-singularity --singularity-args "--bind /mnt/disk1 --bind /mnt/disk2" -p --jobs 12 --configfile configP1.yml > logs/runP1.stdout 2> logs/runP1.stderr
snakemake --use-conda --use-singularity --singularity-args "--bind /mnt/disk1 --bind /mnt/disk2" -p --jobs 12 --configfile configP2.yml > logs/runP2.stdout 2> logs/runP2.stderr
snakemake --use-conda --use-singularity --singularity-args "--bind /mnt/disk1 --bind /mnt/disk2" -p --jobs 12 --configfile configP3.yml > logs/runP3.stdout 2> logs/runP3.stderr
snakemake --use-conda --use-singularity --singularity-args "--bind /mnt/disk1 --bind /mnt/disk2" -p --jobs 12 --configfile config2041.yml > logs/run2041.stdout 2> logs/run2041.stderr
```

## Merging fastq-files afterwards

```bash
snakemake -p --use-conda --use-singularity --singularity-args "--bind /mnt/disk2" --snakefile SnakefileMergeFQ --configfile config-merge.yaml --jobs 12 > logs/run.stdout.merge 2> logs/run.stderr.merge
```
